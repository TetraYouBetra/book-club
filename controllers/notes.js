const { Book } = require('../models/book');
const { Note } = require('../models/note');
const { User } = require('../models/user');
const stringToBoolean = require('../utils/stringToBoolean');


module.exports.newNote = async (req, res, next) => {
    try {
        const data = req.body;
        const bookId = data["newcomment-parentBookId"];
        let parentbook = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        }).populate({
            path: 'club'
        });
        if (parentbook) {
            if (parentbook.club.private) {
                if (req.user) {
                    if (parentbook.club.members.indexOf(req.user._id) == -1) {
                        throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                    }
                } else {
                    throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                }
            }

            const note = new Note({
                author: req.user._id,
                body: data["newcomment-body"],
                parentBook: bookId,
                posted: new Date(),
                siQuote: data["newcomment-siQuote"],
                siPage: data["newcomment-siPage"]
            });
            await note.save();

            const book = Book.findByIdAndUpdate(bookId,
                { $push: { "notes": note._id } },
                { safe: true, upsert: true },
                function (err, model) {
                    console.log(err);
                }
            );

            const user = User.findByIdAndUpdate(req.user._id,
                { $push: { "notes": note._id } },
                { safe: true, upsert: true },
                function (err, model) {
                    console.log(err);
                }
            );

            res.json({ status: 201, message: "Successfully posted your note!", newid: note._id });
            // res.json({ status: 201, message: "Successfully posted your comment!", data: { bookid: book.id } });
        } else {
            throw { status: 403, message: "The book you're posting on doesn't appear to exist!" };
        }
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getEditData = async (req, res, next) => {
    try {
        const { noteId } = req.params;

        const note = await Note.findOne({
            _id: noteId,
            deleted: { $ne: true }
        });

        if (note) {
            if (req.user._id.toString() != note.author.toString()) {
                throw { status: 403, message: 'You cannot edit this note.' };
            }

            res.json({
                status: 200, note: {
                    type: "Note",
                    body: note.body,
                    siQuote: note.siQuote,
                    siPage: note.siPage,
                    id: note._id
                }
            })
        } else {
            throw { status: 404, message: '404 note not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.editNote = async (req, res, next) => {
    try {
        const data = req.body;
        const { noteId } = req.params;

        const note = await Note.findOne({
            _id: noteId,
            deleted: { $ne: true }
        });

        if (note) {
            if (req.user._id.toString() != note.author.toString()) {
                throw { status: 403, message: 'You cannot edit this note.' };
            }

            note.body = data["newcomment-body"];
            note.siQuote = data["newcomment-siQuote"];
            note.siPage = data["newcomment-siPage"];
            await note.save();

            res.json({ status: 200, message: 'Successfully edited note.', newid: note._id });
        } else {
            throw { status: 404, message: '404 note not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.deleteNote = async (req, res, next) => {
    try {
        const { noteId } = req.params;

        const note = await Note.findOne({
            _id: noteId,
            deleted: { $ne: true }
        });

        if (note) {
            if (req.user._id.toString() != note.author.toString()) {
                throw { status: 403, message: 'You cannot delete this note.' };
            }

            note.deleted = true;
            await note.save()

            res.json({ status: 200, message: "Successfully deleted the note." })
        } else {
            throw { status: 404, message: '404 note not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}