const { Book } = require('../models/book');
const { Club } = require('../models/club');
const { Alert } = require('../models/alert');
const { User } = require('../models/user');
const { cloudinary } = require("../cloudinary");
const stringToBoolean = require('../utils/stringToBoolean');
const { sendAlert } = require('../utils/sendEmail');

module.exports.newBook = async (req, res, next) => {
    try {
        const data = req.body;

        let newIcon = { url: data["newbook-altCover"], filename: data["newbook-altCover"] };

        if (req.file) {
            newIcon = { url: req.file.path, filename: req.file.filename };
            req.flash('success', 'Updated your display picture.');
        }

        const book = new Book({
            club: data["newbook-clubId"],
            olId: data["newbook-olId"],
            aId: data["newbook-aId"],
            title: data["newbook-title"],
            description: data["newbook-description"],
            coverThumb: newIcon,
            author: data["newbook-author"],
            poster: req.user._id,
            publishedDate: data["newbook-published"],
            posted: new Date(), // first posted
            updated: new Date(), // last updated
            tags: data["newbook-tags"].split(",").filter(e => e),
            likes: [], //[req.user._id],
            comments: [],
            notes: [],
        });
        await book.save();

        const club = await Club.findByIdAndUpdate(data["newbook-clubId"],
            { $push: { "books": book._id } },
            { safe: true, upsert: true, new: true }
        ).populate('members').exec();

        const user = User.findByIdAndUpdate(req.user._id,
            { $push: { "books": book._id } },
            { safe: true, upsert: true },
            function (err, model) {
                console.log(err);
            }
        );

        // alerting - this will alert all members of the club when a book is posted
        club.members.forEach(async (member, index) => {
            if (member._id.toString() !== req.user._id.toString() && member.settings.alerts.books && (club.private || member.settings.alertpublic)) {
                const alert = new Alert({
                    user: member._id,
                    refBook: book._id,
                    refClub: club._id,
                    refUser: req.user._id,
                    icon: "booklogo",
                    code: "book_new",
                    posted: new Date(),
                });
                await alert.save();

                const alertPoster = await User.findOneAndUpdate({ _id: member._id },
                    { $push: { "alerts": alert._id } },
                    { safe: true, upsert: true, new: true }
                ).exec();
            }
        });


        res.json({ status: 201, message: "Successfully posted your book!", data: { bookid: book.id } });
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getBooks = async (req, res, next) => {
    try {
        // console.log(req)
        const { q = "_New", c } = req.query;

        const userClubIds = [];
        res.locals.clubs.forEach((club, index) => {
            userClubIds.push(club._id);
        });
        // bookFilter to be used to filter Saved/Bookmarked books
        let bookFilter = {
            'deleted': {
                $ne: true
            }
        };
        let clubFilter = {
            'deleted': {
                $ne: true
            }
        };
        if (c) {
            clubFilter = {
                'deleted': {
                    $ne: true
                },
                _id: c
            };
        }
        let combinedBooks = [];
        let combinedMeetings = [];

        if (req.user && req.user.clubs.length > 0) {
            if (q == "_Saved") {
                // bookmarked books
                bookFilter = {
                    'deleted': {
                        $ne: true
                    },
                    bookmarks: {
                        $in: req.user._id
                    }
                };
            }
            let user = await User.findById(req.user._id).populate({
                path: 'clubs',
                populate: {
                    path: 'books',
                    populate: [{
                        path: 'poster club'
                    }, {
                        path: 'comments',
                        select: '_id',
                        populate: {
                            path: 'replies',
                            select: '_id',
                        },
                        match: {
                            'deleted': {
                                $ne: true
                            }
                        }
                    }, {
                        path: 'meetings',
                        populate: {
                            path: 'parentBook',
                            populate: {
                                path: 'club',
                                select: 'color'
                            },
                        },
                        match: {
                            'deleted': {
                                $ne: true
                            }
                        }
                    }, {
                        path: 'polls',
                        match: {
                            'deleted': {
                                $ne: true
                            }
                        }
                    }],
                    match: bookFilter
                },
                match: clubFilter
            });
            user.clubs.forEach((club, index) => {
                club.books.forEach((book, index) => {
                    combinedBooks.push(book);
                });
            });
        } else {
            const publicClubs = await Club.find({ private: false, deleted: { $ne: true } }).limit(10).populate({
                path: 'books',
                populate: [{
                    path: 'poster club'
                }, {
                    path: 'comments',
                    select: '_id',
                    populate: {
                        path: 'replies',
                        select: '_id',
                    },
                    match: {
                        'deleted': {
                            $ne: true
                        }
                    }
                }, {
                    path: 'meetings',
                    populate: {
                        path: 'parentBook',
                        populate: {
                            path: 'club',
                            select: 'color'
                        },
                    },
                    match: {
                        'deleted': {
                            $ne: true
                        }
                    }
                }, {
                    path: 'polls',
                    match: {
                        'deleted': {
                            $ne: true
                        }
                    }
                }],
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            });
            publicClubs.forEach((club, index) => {
                club.books.forEach((book, index) => {
                    combinedBooks.push(book);
                });
            });
        }
        switch (q) {
            // sort by newest
            case "_New": combinedBooks.sort((a, b) => a.posted < b.posted && 1 || -1);
                break;
            // sort by likes
            case "_Top": combinedBooks.sort((a, b) => a.likes.length < b.likes.length && 1 || -1);
                break;
            // sort by updated
            case "_Activity": combinedBooks.sort((a, b) => a.updated < b.updated && 1 || -1);
                break;
        }

        //combine the meetings
        combinedBooks.forEach((book, index) => {
            book.meetings.forEach((meeting, index) => {
                combinedMeetings.push(meeting);
            });
        });

        res.renderWithData('layouts/books', { expanded: false, books: combinedBooks, meetings: combinedMeetings, q }, { status: 200 });
    } catch (e) {
        console.log(e)
        res.renderWithData('layouts-popout/error', { e }, { slug: "not_found", topcolor: "#FF6961", bookmark: false });
    }
}

module.exports.getBook = async (req, res, next) => {
    try {
        const { bookId } = req.params;
        // const result = books.filter((book) => book.id == bookId);
        let book;
        let bookmark = false;

        if (req.user) {
            book = await Book.findOne({ _id: bookId, deleted: { $ne: true } }).populate([{
                path: 'comments',
                populate: {
                    path: 'author'
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }, {
                path: 'comments',
                populate: {
                    path: 'replies',
                    populate: {
                        path: 'author',
                    },
                    match: {
                        'deleted': {
                            $ne: true
                        }
                    }
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }]).populate({
                path: 'club'
            }).populate({ path: 'poster' }).populate({
                path: 'notes',
                populate: {
                    path: 'author',
                },
                match: {
                    'author': {
                        $eq: req.user._id
                    },
                    'deleted': {
                        $ne: true
                    }
                },
            }).populate({
                path: 'meetings',
                populate: [{
                    path: 'author',
                }, {
                    path: 'parentBook',
                    populate: {
                        path: 'club',
                        select: 'color'
                    },
                }],
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }).populate({
                path: 'polls',
                populate: {
                    path: 'author',
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            });

            if (book.bookmarks.indexOf(req.user._id) !== -1) {
                bookmark = true;
            }
        } else {
            book = await Book.findOne({ _id: bookId, deleted: { $ne: true } }).populate([{
                path: 'comments',
                populate: {
                    path: 'author'
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }, {
                path: 'comments',
                populate: {
                    path: 'replies',
                    populate: {
                        path: 'author',
                    },
                    match: {
                        'deleted': {
                            $ne: true
                        }
                    }
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }]).populate({
                path: 'club',
            }).populate({ path: 'poster' }).populate({
                path: 'meetings',
                populate: [{
                    path: 'author',
                }, {
                    path: 'parentBook',
                    populate: {
                        path: 'club',
                        select: 'color'
                    },
                }],
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }).populate({
                path: 'polls',
                populate: {
                    path: 'author',
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            });
        }

        let slug = "";
        let topcolor = "";

        if (book) {
            if (book.club.private) {
                if (req.user) {
                    if (book.club.members.indexOf(req.user._id) == -1) {
                        throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                    }
                } else {
                    throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                }
            }
            slug = book.slug;
            topcolor = book.club.color;

            res.renderWithData('layouts-popout/book', { expanded: true, books: [book] }, { slug: slug, topcolor: topcolor, bookmark: bookmark });
        } else {
            throw { status: 404, message: "The page or item you're looking for doesn't exist!" };
        }
    } catch (e) {
        console.log(e)
        res.renderWithData('layouts-popout/error', { e }, { slug: "not_found", topcolor: "#FF6961", bookmark: false });
    }
}


module.exports.likeBook = async (req, res, next) => {
    try {
        const { bookId } = req.params;

        const book = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        }).populate({
            path: 'club'
        });

        if (book) {
            if (book.club.members.indexOf(req.user._id) !== -1) {
                if (book.likes.indexOf(req.user._id) !== -1) {
                    // user likes book already, remove user
                    const updatebook = await Book.findByIdAndUpdate(bookId,
                        { $pull: { "likes": req.user._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    res.json({ status: 200, evtcode: "unliked", newlikes: updatebook.likes.length, message: "Successfully unliked the book." });
                } else {
                    // user in not in club, add if not private
                    // if private only join if code supplied and matches
                    const updatebook = await Book.findByIdAndUpdate(bookId,
                        { $push: { "likes": req.user._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    res.json({ status: 200, evtcode: "liked", newlikes: updatebook.likes.length, message: "Successfully liked the book." });
                }
            } else {
                throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
            }
        } else {
            throw { status: 404, message: '404 club not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}


module.exports.bookmarkBook = async (req, res, next) => {
    try {
        const { bookId } = req.params;

        const book = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        }).populate({
            path: 'club'
        });

        if (book) {
            if (book.club.members.indexOf(req.user._id) !== -1) {
                if (book.bookmarks.indexOf(req.user._id) !== -1) {
                    // user likes book already, remove user
                    const updatebook = await Book.findByIdAndUpdate(bookId,
                        { $pull: { "bookmarks": req.user._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    const updateuser = await User.findByIdAndUpdate(req.user._id,
                        { $pull: { "bookmarks": book._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    res.json({ status: 200, evtcode: "unbookmarked", message: "Successfully unbookmarked the book." });
                } else {
                    // user in not in club, add if not private
                    // if private only join if code supplied and matches
                    const updatebook = await Book.findByIdAndUpdate(bookId,
                        { $push: { "bookmarks": req.user._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    const updateuser = await User.findByIdAndUpdate(req.user._id,
                        { $push: { "bookmarks": book._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    res.json({ status: 200, evtcode: "bookmarked", message: "Successfully bookmarked the book." });
                }
            } else {
                throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
            }
        } else {
            throw { status: 404, message: '404 book not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getEditData = async (req, res, next) => {
    try {
        const { bookId } = req.params;

        const book = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        });

        if (book) {
            if (req.user._id.toString() != book.poster.toString()) {
                throw { status: 403, message: 'You cannot edit this book.' };
            }

            res.json({
                status: 200, book: {
                    type: "Book",
                    title: book.title,
                    description: book.description,
                    coverThumb: book.coverThumb,
                    author: book.author,
                    publishedDate: book.publishedDate,
                    tags: book.tags,
                    olId: book.olId,
                    aId: book.aId,
                }
            })
        } else {
            throw { status: 404, message: '404 book not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.editBook = async (req, res, next) => {
    try {
        const data = req.body;
        const { bookId } = req.params;

        const book = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        });

        if (book) {
            if (req.user._id.toString() != book.poster.toString()) {
                throw { status: 403, message: 'You cannot edit this book.' };
            }

            if (req.file) {
                await cloudinary.uploader.destroy(book.coverThumb.filename);
                book.coverThumb.url = req.file.path;
                book.coverThumb.filename = req.file.filename;
            }

            book.olId = data["newbook-olId"];
            book.aId = data["newbook-aId"];
            book.title = data["newbook-title"];
            book.description = data["newbook-description"];
            book.author = data["newbook-author"];
            book.publishedDate = data["newbook-published"];
            book.tags = data["newbook-tags"].split(",").filter(e => e);
            book.updated = new Date();

            await book.save();
            res.json({ status: 201, message: "Successfully updated your book!", data: { bookid: book.id } });
        } else {
            throw { status: 404, message: '404 book not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.deleteBook = async (req, res, next) => {
    try {
        const { bookId } = req.params;

        const book = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        });

        if (book) {
            if (req.user._id.toString() != book.poster.toString()) {
                throw { status: 403, message: 'You cannot delete this book.' };
            }

            book.deleted = true;
            await book.save()

            res.json({ status: 200, message: "Successfully deleted the book." })
        } else {
            throw { status: 404, message: '404 book not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}