const { Book } = require('../models/book');
const { Poll } = require('../models/poll');
const { User } = require('../models/user');
const { Alert } = require('../models/alert');
const stringToBoolean = require('../utils/stringToBoolean');


module.exports.newPoll = async (req, res, next) => {
    try {
        const data = req.body;

        const bookId = data["newcomment-parentBookId"];
        let parentbook = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        }).populate({
            path: 'club'
        });
        if (parentbook) {
            if (parentbook.club.private) {
                if (req.user) {
                    if (parentbook.club.members.indexOf(req.user._id) == -1) {
                        throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                    }
                } else {
                    throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                }
            }

            const poll = new Poll({
                author: req.user._id,
                parentBook: bookId,
                posted: new Date(),
                question: data['poll-question'],
                answers: data['poll-answers'],
                multiplechoice: stringToBoolean(data['poll-multiplechoice'])
            });
            await poll.save();

            const book = Book.findByIdAndUpdate(bookId,
                { $push: { "polls": poll._id }, updated: new Date() },
                { safe: true, upsert: true },
                function (err, model) {
                    console.log(err);
                }
            );

            const user = User.findByIdAndUpdate(req.user._id,
                { $push: { "polls": poll._id } },
                { safe: true, upsert: true },
                function (err, model) {
                    console.log(err);
                }
            );

            // alerting - this will alert all members of the club when a poll is posted
            const alertBookLookup = await Book.findOne({
                _id: bookId,
                deleted: { $ne: true }
            }).populate({
                path: 'club',
                populate: 'members'
            });
            alertBookLookup.club.members.forEach(async (member, index) => {
                if (member._id.toString() !== req.user._id.toString() && member.settings.alerts.polls && (alertBookLookup.club.private || member.settings.alertpublic)) {
                    const alert = new Alert({
                        user: member._id,
                        refBook: bookId,
                        refPoll: poll._id,
                        refUser: req.user._id,
                        icon: "barchart-line",
                        code: "poll_new",
                        posted: new Date(),
                    });
                    await alert.save();

                    const alertPoster = await User.findOneAndUpdate({ _id: member._id },
                        { $push: { "alerts": alert._id } },
                        { safe: true, upsert: true, new: true }
                    ).exec();
                }
            });


            res.json({ status: 201, message: "Successfully posted your poll!", newid: poll._id });
            // res.json({ status: 201, message: "Successfully posted your comment!", data: { bookid: book.id } });
        } else {
            throw { status: 403, message: "The book you're posting on doesn't appear to exist!" };
        }
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.voteOnPoll = async (req, res, next) => {
    try {
        const { pollId } = req.params;
        const { votes } = req.body;

        let poll = await Poll.findOne({ _id: pollId, deleted: { $ne: true } }).populate({
            path: 'parentBook',
            populate: {
                path: 'club',
            },
        });
        if (poll) {
            if (poll.parentBook.club.members.indexOf(req.user._id) == -1 && poll.parentBook.club.private) {
                throw { status: 403, message: 'You must be a member of this club to vote.' };
            }
            if (poll.voters.indexOf(req.user._id) !== -1) {
                throw { status: 400, message: 'You have already voted on this poll.' };
            }
            if (votes.length > 1 && !poll.multiplechoice) {
                throw { status: 400, message: 'This is not a multiple choice poll.' };
            }

            if (votes) {
                let votesuccessful = false;
                votes.forEach((vote, index) => {
                    poll.answers.forEach((answer, index) => {
                        if (answer._id == vote) {
                            poll.answers[index].votes.push(req.user._id);
                            votesuccessful = true;
                        }
                    });
                });
                if (votesuccessful) {
                    poll.voters.push(req.user._id);
                }
            }

            await poll.save();

            res.json({ status: 201, evtcode: "voted", message: "Successfully posted your poll answer!" });
            // res.json({ status: 201, message: "Successfully posted your comment!", data: { bookid: book.id } });
        } else {
            throw { status: 404, message: "The poll you're voting on doesn't appear to exist." };
        }
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getEditData = async (req, res, next) => {
    try {
        const { pollId } = req.params;

        const poll = await Poll.findOne({
            _id: pollId,
            deleted: { $ne: true }
        });

        if (poll) {
            if (req.user._id.toString() != poll.author.toString()) {
                throw { status: 403, message: 'You cannot edit this poll.' };
            }

            res.json({
                status: 200, poll: {
                    type: "Poll",
                    question: poll.question,
                    answers: poll.answers,
                    multiplechoice: poll.multiplechoice,
                    id: poll._id
                }
            })
        } else {
            throw { status: 404, message: '404 poll not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.editPoll = async (req, res, next) => {
    try {
        const data = req.body;
        const { pollId } = req.params;
        let newAnswers = data['poll-answers'];

        const poll = await Poll.findOne({
            _id: pollId,
            deleted: { $ne: true }
        });

        if (poll) {
            if (req.user._id.toString() != poll.author.toString()) {
                throw { status: 403, message: 'You cannot edit this poll.' };
            }

            poll.question = data['poll-question'];
            poll.multiplechoice = stringToBoolean(data['poll-multiplechoice']);
            newAnswers.forEach((newAnswer, newindex) => {
                for (let i = 0; i < poll.answers.length; i++) {
                    if (poll.answers[i]._id.toString() == newAnswer.id) {
                        newAnswers[newindex].votes = poll.answers[i].votes;
                    }
                }
            });
            poll.answers = newAnswers;
            await poll.save();

            res.json({ status: 201, message: "Successfully edited your poll!", newid: poll._id });
        } else {
            throw { status: 404, message: '404 poll not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.deletePoll = async (req, res, next) => {
    try {
        const { pollId } = req.params;

        const poll = await Poll.findOne({
            _id: pollId,
            deleted: { $ne: true }
        });

        if (poll) {
            if (req.user._id.toString() != poll.author.toString()) {
                throw { status: 403, message: 'You cannot delete this poll.' };
            }

            poll.deleted = true;
            await poll.save()

            res.json({ status: 200, message: "Successfully deleted the poll." })
        } else {
            throw { status: 404, message: '404 poll not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}