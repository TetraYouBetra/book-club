const { Club } = require('../models/club');
const { User } = require('../models/user');
const { Alert } = require('../models/alert');
const { cloudinary } = require("../cloudinary");
const stringToBoolean = require('../utils/stringToBoolean');


module.exports.newClub = async (req, res, next) => {
    try {
        const data = req.body;

        let newIcon = {};
        if (req.file) {
            newIcon = { url: req.file.path, filename: req.file.filename };
        }

        const club = new Club({
            name: data["newclub-name"],
            created: new Date(),
            color: data["newclub-color"],
            description: data["newclub-description"],
            icon: newIcon,
            private: stringToBoolean(data["newclub-private"]),
            owners: [req.user._id],
            members: [req.user._id],
        });
        await club.save();

        const user = User.findByIdAndUpdate(req.user._id,
            { $push: { "clubs": club._id } },
            { safe: true, upsert: true },
            function (err, model) {
                console.log(err);
            }
        );

        res.json({ status: 201, message: "Successfully created your club!", data: { clubid: club.id } });
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.prejoinClub = async (req, res, next) => {
    try {
        const { clubId } = req.params;
        const { joinCode } = req.query;

        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        })

        res.renderWithData('layouts-minipopout/prejoin', { joinCode: joinCode, id: clubId, club: club }, {});

    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.joinClub = async (req, res, next) => {
    try {
        const { clubId } = req.params;
        const { joinCode } = req.body;

        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        })
        if (club) {
            if (club.members.indexOf(req.user._id) !== -1) {
                // user is in club already, remove user
                if (club.owners.indexOf(req.user._id) !== -1) {
                    throw { message: "You cannot leave clubs you own." };
                }

                const updateclub = Club.findByIdAndUpdate(clubId,
                    { $pull: { "members": req.user._id } },
                    { safe: true, upsert: true },
                    function (err, model) {
                        console.log(err);
                    }
                );
                const user = User.findByIdAndUpdate(req.user._id,
                    { $pull: { "clubs": club._id } },
                    { safe: true, upsert: true },
                    function (err, model) {
                        console.log(err);
                    }
                );
                res.json({ status: 200, evtcode: "left", message: "Successfully left the club." });
            } else {
                // user in not in club, add if not private
                // if private only join if code supplied and matches
                if (!club.private || (club.private == true && joinCode == club.joinCode)) {
                    const updateclub = await Club.findByIdAndUpdate(clubId,
                        { $push: { "members": req.user._id } },
                        { safe: true, upsert: true, new: true }
                    ).populate('members').exec();

                    const user = User.findByIdAndUpdate(req.user._id,
                        { $push: { "clubs": club._id } },
                        { safe: true, upsert: true },
                        function (err, model) {
                            console.log(err);
                        }
                    );

                    // alerting - this will alert all members of the club when a user joins
                    // only if private
                    updateclub.members.forEach(async (member, index) => {
                        if (member._id.toString() !== req.user._id.toString() && member.settings.alerts.newmembers && (updateclub.private || member.settings.alertpublic)) {
                            const alert = new Alert({
                                user: member._id,
                                refClub: club._id,
                                refUser: req.user._id,
                                icon: "grid",
                                code: "club_joined",
                                posted: new Date(),
                            });
                            await alert.save();

                            const alertPoster = await User.findOneAndUpdate({ _id: member._id },
                                { $push: { "alerts": alert._id } },
                                { safe: true, upsert: true, new: true }
                            ).exec();
                        }
                    });


                    res.json({ status: 200, evtcode: "joined", message: "Successfully joined the club." });
                } else {
                    throw { message: "Failed to join, incorrect club or code." };
                }
            }
        } else {
            throw { message: "404 club not found" };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getClubs = async (req, res, next) => {
    const publicClubs = await Club.find({ private: false, deleted: { $ne: true } }).limit(10);
    // club data comes from a middleware
    res.renderWithData('layouts/clubs', { expanded: false, publicClubs: publicClubs }, { status: 200 });
}

module.exports.getClub = async (req, res, next) => {
    try {
        const { q = "_New" } = req.query;
        const { clubId } = req.params;

        let combinedBooks = [];
        let combinedMeetings = [];
        // bookFilter to be used to filter Saved/Bookmarked books
        let bookFilter = {
            'deleted': {
                $ne: true
            }
        };
        if (req.user) {
            if (q == "_Saved") {
                // bookmarked books
                bookFilter = {
                    'deleted': {
                        $ne: true
                    },
                    bookmarks: {
                        $in: req.user._id
                    }
                };
            }
        }
        // const result = bookClubs.filter((club) => club.id == clubId);
        // const resultBooks = books.filter((book) => book.club.id == clubId);

        // let club = await Club.findById(clubId).populate({
        let club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true },
            // members: { $elemMatch: { $eq: req.user._id } }
        }).populate({
            path: 'books',
            populate: [{
                path: 'poster club',
            }, {
                path: 'comments',
                select: '_id',
                populate: {
                    path: 'replies',
                    select: '_id',
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }, {
                path: 'meetings',
                populate: {
                    path: 'parentBook',
                    populate: {
                        path: 'club',
                        select: 'color'
                    },
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }, {
                path: 'polls',
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            }],
            match: bookFilter
        });
        let slug = "";
        let topcolor = "";

        // res.render('components/book-post', { expanded: true, books: result });
        if (club) {
            if (club.private) {
                if (req.user) {
                    if (club.members.indexOf(req.user._id) == -1) {
                        throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                    }
                } else {
                    throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                }
            }
            slug = club.slug;
            topcolor = club.color;

            club.books.forEach((booki, index) => {
                combinedBooks.push(booki);
            });

            switch (q) {
                // sort by newest
                case "_New": combinedBooks.sort((a, b) => a.posted < b.posted && 1 || -1);
                    break;
                // sort by likes
                case "_Top": combinedBooks.sort((a, b) => a.likes.length < b.likes.length && 1 || -1);
                    break;
                // sort by updated
                case "_Activity": combinedBooks.sort((a, b) => a.updated < b.updated && 1 || -1);
                    break;
            }

            //combine the meetings
            combinedBooks.forEach((book, index) => {
                book.meetings.forEach((meeting, index) => {
                    combinedMeetings.push(meeting);
                });
            });

            res.renderWithData('layouts-popout/club', { expanded: true, club: club, books: combinedBooks, meetings: combinedMeetings, q }, { slug: slug, topcolor: topcolor, bookmark: false });
        } else {
            throw { status: 404, message: "The page or item you're looking for doesn't exist!" };
        }
    } catch (e) {
        console.log(e)
        res.renderWithData('layouts-popout/error', { e }, { slug: "not_found", topcolor: "#FF6961", bookmark: false });
    }
}

module.exports.getClubInvitation = async (req, res, next) => {
    try {
        const { clubId } = req.params;
        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        })
        if (club) {
            if (club.private) {
                if (req.user) {
                    if (club.members.indexOf(req.user._id) == -1) {
                        throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                    }
                } else {
                    throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                }
            }
            club.joinCode = makeCode();
            await club.save();

            res.renderWithData('layouts-minipopout/share', { joinCode: club.joinCode, joinUrl: `https://bookclub.tetrayoubetra.com/Clubs/${club._id}/join?joinCode=${club.joinCode}`, id: club._id }, {});
        } else {
            throw { status: 404, message: "The page or item you're looking for doesn't exist!" };
        }
    } catch (e) {
        console.log(e)
        res.renderWithData('layouts-popout/error', { e }, { slug: "not_found", topcolor: "#FF6961", bookmark: false });
    }
}

function makeCode() {
    const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let result = "";
    for (var i = 0; i < 6; i++) {
        result += chars.charAt(Math.floor(Math.random() *
            chars.length));
    }
    return result;
}

module.exports.getEditData = async (req, res, next) => {
    try {
        const { clubId } = req.params;

        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        });

        if (club) {
            if (club.owners.indexOf(req.user._id) == -1) {
                throw { status: 403, message: 'You cannot edit this club.' };
            }

            res.json({
                status: 200, club: {
                    type: "Club",
                    name: club.name,
                    color: club.color,
                    description: club.description,
                    icon: club.icon.url,
                    private: club.private
                }
            })
        } else {
            throw { status: 404, message: '404 club not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getMemberList = async (req, res, next) => {
    try {
        const { clubId } = req.params;

        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        }).populate('members');

        if (club) {
            if (club.owners.indexOf(req.user._id) == -1 && !club.private) {
                throw { status: 403, message: 'Only club owners can manage members.' };
            }

            res.renderWithData('layouts-minipopout/clubmembers', { id: clubId, club: club }, { status: 200, id: clubId });

        } else {
            throw { status: 404, message: '404 club not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.updateMember = async (req, res, next) => {
    try {
        const { clubId } = req.params;
        const { memberId, action } = req.body;

        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        });

        let message = "Nothing was updated.";
        let evtcode = "";

        if (club) {
            if (club.owners.indexOf(req.user._id) == -1) {
                throw { status: 403, message: 'Only club owners can manage members.' };
            }
            if (memberId == req.user._id.toString() && club.owners.length < 2) {
                throw { status: 403, message: 'You must promote a member before you can demote yourself.' };
            }

            if (action == "kick") {
                const updateclub = await Club.findByIdAndUpdate(clubId,
                    { $pull: { "members": memberId, "owners": memberId } },
                    { safe: true, upsert: true }
                ).exec();
                const user = await User.findByIdAndUpdate(memberId,
                    { $pull: { "clubs": club._id } },
                    { safe: true, upsert: true }
                ).exec();
                message = `${user.displayname} kicked from club.`;
                evtcode = "kicked";
            }
            if (action == "promote") {
                let query = {};
                const user = await User.findById(memberId);

                if (club.owners.indexOf(memberId) == -1) {
                    query = { $push: { "owners": memberId } };
                    message = `${user.displayname} promoted to club owner.`;
                    evtcode = "promoted";
                } else {
                    query = { $pull: { "owners": memberId } };
                    message = `${user.displayname} demoted from club owner.`;
                    evtcode = "demoted";
                }
                const updateclub = await Club.findByIdAndUpdate(clubId,
                    query,
                    { safe: true, upsert: true }
                ).exec();
            }

            res.json({ status: 200, message: message, evtcode: evtcode });

        } else {
            throw { status: 404, message: '404 club not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.editClub = async (req, res, next) => {
    try {
        const data = req.body;
        const { clubId } = req.params;

        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        });

        if (club) {
            if (club.owners.indexOf(req.user._id) == -1) {
                throw { status: 403, message: 'You cannot edit this club.' };
            }

            if (req.file) {
                await cloudinary.uploader.destroy(club.icon.filename);
                club.icon.url = req.file.path;
                club.icon.filename = req.file.filename;
            }

            club.name = data["newclub-name"];
            club.color = data["newclub-color"];
            club.description = data["newclub-description"];
            club.private = stringToBoolean(data["newclub-private"]);

            await club.save();
            res.json({ status: 201, message: "Successfully updated your club!", data: { clubid: club.id } });
        } else {
            throw { status: 404, message: '404 club not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}


module.exports.deleteClub = async (req, res, next) => {
    try {
        const { clubId } = req.params;

        const club = await Club.findOne({
            _id: clubId,
            deleted: { $ne: true }
        });

        if (club) {
            if (club.owners.indexOf(req.user._id) == -1) {
                throw { status: 403, message: 'You cannot delete this club.' };
            }

            club.deleted = true;
            await club.save()

            res.json({ status: 200, message: "Successfully deleted the club." })
        } else {
            throw { status: 404, message: '404 club not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}