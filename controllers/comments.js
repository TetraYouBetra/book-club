const { Book } = require('../models/book');
const { Comment } = require('../models/comment');
const { Alert } = require('../models/alert');
const { User } = require('../models/user');
const stringToBoolean = require('../utils/stringToBoolean');


module.exports.newComment = async (req, res, next) => {
    try {
        const data = req.body;
        const bookId = data["newcomment-parentBookId"];
        let parentbook = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        }).populate({
            path: 'club'
        });

        if (parentbook) {
            if (parentbook.club.private) {
                if (req.user) {
                    if (parentbook.club.members.indexOf(req.user._id) == -1) {
                        throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                    }
                } else {
                    throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                }
            }

            const comment = new Comment({
                author: req.user._id,
                body: data["newcomment-body"],
                replies: [],
                parentBook: bookId,
                parentComment: data["newcomment-parentCommentId"],
                posted: new Date(),
                updated: new Date(),
                likes: [], //[req.user._id],
                siQuote: data["newcomment-siQuote"],
                siPage: data["newcomment-siPage"]
            });
            await comment.save();

            if (data["newcomment-postToType"] == "Comment") {
                const book = await Book.findByIdAndUpdate(bookId,
                    { $push: { "comments": comment._id }, updated: new Date() },
                    { safe: true, upsert: true, new: true }
                ).populate('poster').exec();

                // alerting
                if (book.poster._id.toString() !== req.user._id.toString() && book.poster.settings.alerts.comments) {
                    const alert = new Alert({
                        user: book.poster._id,
                        refBook: bookId,
                        refComment: comment._id,
                        refUser: req.user._id,
                        icon: "comments",
                        code: "comment_new",
                        posted: new Date(),
                    });
                    await alert.save();

                    const alertPoster = await User.findOneAndUpdate({ _id: book.poster._id },
                        { $push: { "alerts": alert._id } },
                        { safe: true, upsert: true, new: true }
                    ).exec();
                }
            } else if (data["newcomment-postToType"] == "Reply") {
                const parentComment = await Comment.findByIdAndUpdate(data["newcomment-parentCommentId"],
                    { $push: { "replies": comment._id }, updated: new Date() },
                    { safe: true, upsert: true, new: true }
                ).populate('author').exec();

                // alerting
                if (parentComment.author._id.toString() !== req.user._id.toString() && parentComment.author.settings.alerts.replies) {
                    const alert = new Alert({
                        user: parentComment.author._id,
                        refBook: bookId,
                        refParentComment: parentComment._id,
                        refComment: comment._id,
                        refUser: req.user._id,
                        icon: "comments",
                        code: "comment_reply",
                        posted: new Date(),
                    });
                    await alert.save();

                    const alertCommentAuthor = await User.findOneAndUpdate({ _id: parentComment.author._id },
                        { $push: { "alerts": alert._id } },
                        { safe: true, upsert: true, new: true }
                    ).exec();
                }
            }

            const user = User.findByIdAndUpdate(req.user._id,
                { $push: { "comments": comment._id } },
                { safe: true, upsert: true },
                function (err, model) {
                    console.log(err);
                }
            );

            res.json({ status: 201, message: "Successfully posted your comment!", newid: comment._id });
        } else {
            throw { status: 403, message: "The book you're commenting on doesn't appear to exist!" };
        }
    } catch (e) {
        res.json({ status: 500, message: e.message });
        console.log(e)
    }
}

module.exports.likeComment = async (req, res, next) => {
    try {
        const { commentId } = req.params;

        const comment = await Comment.findOne({
            _id: commentId,
            deleted: { $ne: true }
        }).populate({
            path: 'parentBook',
            populate: {
                path: 'club',
            },
        });

        if (comment) {
            if (comment.parentBook.club.members.indexOf(req.user._id) !== -1) {
                if (comment.likes.indexOf(req.user._id) !== -1) {
                    const updatecomment = await Comment.findByIdAndUpdate(commentId,
                        { $pull: { "likes": req.user._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    res.json({ status: 200, evtcode: "unliked", newlikes: updatecomment.likes.length, message: "Successfully unliked the comment." });
                } else {
                    const updatecomment = await Comment.findByIdAndUpdate(commentId,
                        { $push: { "likes": req.user._id } },
                        { safe: true, upsert: true, new: true }
                    );
                    res.json({ status: 200, evtcode: "liked", newlikes: updatecomment.likes.length, message: "Successfully liked the comment." });
                }
            } else {
                throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
            }
        } else {
            throw { status: 404, message: '404 club not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getEditData = async (req, res, next) => {
    try {
        const { commentId } = req.params;

        const comment = await Comment.findOne({
            _id: commentId,
            deleted: { $ne: true }
        });

        if (comment) {
            if (req.user._id.toString() != comment.author.toString()) {
                throw { status: 403, message: 'You cannot edit this comment.' };
            }

            res.json({
                status: 200, comment: {
                    type: "Comment",
                    body: comment.body,
                    siQuote: comment.siQuote,
                    siPage: comment.siPage,
                    id: comment._id
                }
            })
        } else {
            throw { status: 404, message: '404 comment not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.editComment = async (req, res, next) => {
    try {
        const data = req.body;
        const { commentId } = req.params;

        const comment = await Comment.findOne({
            _id: commentId,
            deleted: { $ne: true }
        });

        if (comment) {
            if (req.user._id.toString() != comment.author.toString()) {
                throw { status: 403, message: 'You cannot edit this comment.' };
            }

            comment.body = data["newcomment-body"];
            comment.siQuote = data["newcomment-siQuote"];
            comment.siPage = data["newcomment-siPage"];
            comment.updated = new Date()
            await comment.save();

            res.json({ status: 200, message: 'Successfully edited comment.', newid: comment._id });
        } else {
            throw { status: 404, message: '404 comment not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.deleteComment = async (req, res, next) => {
    try {
        const { commentId } = req.params;

        const comment = await Comment.findOne({
            _id: commentId,
            deleted: { $ne: true }
        });

        if (comment) {
            if (req.user._id.toString() != comment.author.toString()) {
                throw { status: 403, message: 'You cannot delete this comment.' };
            }

            comment.deleted = true;
            await comment.save()

            res.json({ status: 200, message: "Successfully deleted the comment." })
        } else {
            throw { status: 404, message: '404 comment not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}