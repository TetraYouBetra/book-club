const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const slugify = require('../utils/slugify');

const ImageSchema = new Schema({
    url: String,
    filename: String,
});

const BookSchema = new Schema({
    club: {
        type: Schema.Types.ObjectId,
        ref: 'Club'
    },
    olId: String,
    aId: String,
    title: String,
    description: String,
    coverThumb: ImageSchema,
    author: String,
    poster: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    publishedDate: String,
    posted: Date, // first posted
    updated: Date, // last updated
    tags: [String],
    likes: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    bookmarks: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    notes: [{
        type: Schema.Types.ObjectId,
        ref: 'Note'
    }],
    meetings: [{
        type: Schema.Types.ObjectId,
        ref: 'Meeting'
    }],
    polls: [{
        type: Schema.Types.ObjectId,
        ref: 'Poll'
    }],
    deleted: Boolean
});

// virtuals
BookSchema.virtual("slug").get(function () {
    return slugify(this.title);
});

const Book = mongoose.model('Book', BookSchema);

module.exports = { Book, BookSchema };