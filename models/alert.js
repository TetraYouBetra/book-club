const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlertSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    refBook: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },
    refMeeting: {
        type: Schema.Types.ObjectId,
        ref: 'Meeting'
    },
    refPoll: {
        type: Schema.Types.ObjectId,
        ref: 'Poll'
    },
    refParentComment: {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    },
    refComment: {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    },
    refClub: {
        type: Schema.Types.ObjectId,
        ref: 'Club'
    },
    refUser: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    icon: String,
    code: String,
    posted: Date,
    read: Boolean,
    deleted: Boolean
});

const Alert = mongoose.model('Alert', AlertSchema);

module.exports = { Alert, AlertSchema };