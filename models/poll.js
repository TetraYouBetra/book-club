const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PollSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    parentBook: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },
    question: String,
    answers: [{
        answer: String,
        votes: [{
            type: Schema.Types.ObjectId,
            ref: 'User'
        }]
    }],
    voters: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    multiplechoice: Boolean,
    posted: Date,
    deleted: Boolean
});

const Poll = mongoose.model('Poll', PollSchema);

module.exports = { Poll, PollSchema };