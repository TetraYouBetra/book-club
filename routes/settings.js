const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { User } = require('../models/user');
const settings = require('../controllers/settings');
// display pici  components


router.route('/')
    .get(isLoggedIn, catchAsync(settings.getSettings))
    .patch(isLoggedIn, catchAsync(settings.changeSetting))

module.exports = router;